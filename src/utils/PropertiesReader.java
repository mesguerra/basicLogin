package utils;

/**
 * Created by mee20 on 26/05/2016.
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class PropertiesReader {
    String result = "";
    InputStream inputStream;
    String propFileName = "";
    public PropertiesReader(String propFileName){
    	this.propFileName = propFileName;
    }

	public String getPropValues(String propertie) throws IOException {
        try {
            Properties prop = new Properties();
            inputStream = new FileInputStream(new File(Validador.class.getProtectionDomain().getCodeSource().getLocation().getPath().replace("%20", " ")+ this.propFileName));
            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("Archivo properties '" + propFileName + "' no disponible");
            } 
            result = prop.getProperty(propertie);
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            inputStream.close();
        }
        return result;
    }
}