package servlets;


import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/EditProfileSelect")
public class EditProfileSelect extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7432700501449176526L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("login.jsp");
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession respuesta = request.getSession();
		String id = request.getParameter("id");
		if (id.isEmpty()) {
			respuesta.setAttribute("error", "Se requiere perfil a Editar");
			response.sendRedirect("profilesadmin.jsp");
		} else {
			try {
				respuesta.setAttribute("id", id);
			} catch (Exception e) {
				respuesta.setAttribute(
						"error",
						"Se ha presentado la sigiente excepcion:"
								+ e.toString());
			}

		}
		response.sendRedirect("editprofile.jsp");

	}
}