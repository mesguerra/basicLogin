package servlets;

import beans.*;
import utils.*;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/RegisterUser")
public class RegisterUser extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1876560457614589062L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("registeruser.jsp");
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession respuesta = request.getSession();
		String nombreUsuario = request.getParameter("name");
		String emailUsuario = request.getParameter("email");
		String password = request.getParameter("password1");
		String confirm_password = request.getParameter("password2");
		String profile = request.getParameter("profile");
		String isadmin = request.getParameter("isadmin");
		Validador v = new Validador();
		DaoUser d = new DaoUser();
		if (nombreUsuario.isEmpty() || emailUsuario.isEmpty()
				|| password.isEmpty() || confirm_password.isEmpty()) {
			respuesta.setAttribute("error", "Hay campos vacios");
			response.sendRedirect("registeruser.jsp");
		} else {
			if (v.isUsernameOrPasswordValid(password)) {
				if (password.equals(confirm_password)) {
					try {
						d.conectar();
						if (d.isEmailRegistered(emailUsuario)) {
							respuesta
									.setAttribute("error",
											"Esta direccion de correo ya fue registrada");
							response.sendRedirect("registeruser.jsp");
							
						} else {
							d.registerUser(emailUsuario, password,
									nombreUsuario, profile, isadmin);
							respuesta.setAttribute("error", null);
							response.sendRedirect("usersadmin.jsp");
						}
						d.desconectar();

					} catch (Exception e) {
						respuesta.setAttribute("error", "Ocurrio la sig exception: " + e);
						response.sendRedirect("registeruser.jsp");
					}

				} else {
					respuesta.setAttribute("error",
							"Las contraseņas no son iguales");
					response.sendRedirect("registeruser.jsp");
				}
			} else {
				respuesta.setAttribute("error", "Contraseņa no es valida");
				response.sendRedirect("registeruser.jsp");
			}
		}
		
	}

}