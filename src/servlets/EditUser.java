package servlets;

import beans.*;
import utils.*;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/EditUser")
public class EditUser extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1876560457614589062L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("usersadmin.jsp");
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession respuesta = request.getSession();
		String id = request.getParameter("id");
		String nombreUsuario = request.getParameter("name");
		String emailUsuario = request.getParameter("email");
		String password = request.getParameter("password1");
		String confirm_password = request.getParameter("password2");
		String profile = request.getParameter("profile");
		String isadmin = request.getParameter("isadmin");
		Validador v = new Validador();
		DaoUser d = new DaoUser();
		if (nombreUsuario.isEmpty() || emailUsuario.isEmpty()
				|| password.isEmpty() || confirm_password.isEmpty()) {
			respuesta.setAttribute("error", "Hay campos vacios");
			response.sendRedirect("edituser.jsp");

		} else {

			if (v.isUsernameOrPasswordValid(password)) {
				if (password.equals(confirm_password)) {
					try {
						d.conectar();
						d.updateUser(id, emailUsuario, nombreUsuario, profile,
								isadmin, password);
						respuesta.setAttribute("error", null);
						d.desconectar();
					} catch (Exception e) {
						respuesta.setAttribute("error","Ocurrio la sig exception: " + e);
					}
				} else {
					respuesta.setAttribute("error",
							"Las contraseñas no son iguales");
					response.sendRedirect("edituser.jsp");
				}
			} else {
				respuesta.setAttribute("error", "Contraseña no es válida");
				response.sendRedirect("edituser.jsp");
			}
		}
		response.sendRedirect("usersadmin.jsp");
	}
}