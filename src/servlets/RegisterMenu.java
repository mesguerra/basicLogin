package servlets;

import beans.*;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/RegisterMenu")
public class RegisterMenu extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5931117265405493959L;

	/**
	 * 
	 */

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("registermenu.jsp");
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession respuesta = request.getSession();
		String profile = request.getParameter("profile");
		String[] Programs = request.getParameterValues("programs");
		DaoMenu d = new DaoMenu();
		if (profile.isEmpty() || Programs.length == 0) {
			respuesta.setAttribute("error", "Hay campos vacios");

		} else {
			try {
				d.conectar();
				d.deleteMenu(profile);
				d.registerMenu(profile, Programs);
				respuesta.setAttribute("error", null);
				d.desconectar();
			} catch (Exception e) {
				respuesta.setAttribute("error", "Ocurrio la sig exception: "
						+ e);
			}
		}
		response.sendRedirect("registermenu.jsp");
	}
}
