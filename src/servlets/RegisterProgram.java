package servlets;

import beans.*;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/RegisterProgram")
public class RegisterProgram extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1876560457614589062L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("registerprogram.jsp");
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession respuesta = request.getSession();
		String name = request.getParameter("name").toLowerCase();
		String description = request.getParameter("description");
		DaoProgram d = new DaoProgram();
		if (name.isEmpty() || description.isEmpty()) {
			respuesta.setAttribute("error", "Hay campos vacios");

		} else {
			try {
				d.conectar();
				if (d.isProgramExists(name)) {
					respuesta.setAttribute("error",
							"Este programa ya fue registrado");
				} else {
					d.registerProfile(name, description);
					respuesta.setAttribute("error", null);
				}
				d.desconectar();

			} catch (Exception e) {
				respuesta.setAttribute("error", "Ocurrio la sig exception: " + e);
			}

		}
		response.sendRedirect("registerprogram.jsp");
	}

}
