package servlets;

import beans.DaoUser;
import beans.DaoUserSec;
import utils.Validador;

import java.io.IOException;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/LoginUser")
public class LoginUser extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7432700501449176526L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("login.jsp");
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession respuesta = request.getSession();
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		Validador v = new Validador();
		DaoUser d = new DaoUser();
		if (email.isEmpty() || password.isEmpty()) {
			respuesta.setAttribute("error", "Hay campos vacios");

		} else {
			if (v.isUsernameOrPasswordValid(password)) {
				try {
					d.conectar();
					if (d.isAcountExists(email, password)) {
						ResultSet Usuario = d.getDataByEmail(email);
						respuesta.setAttribute("sessionNombre", Usuario.getString("name"));
						respuesta.setAttribute("sessionEmail", Usuario.getString("email"));
						respuesta.setAttribute("sessionProfile", Usuario.getString("profile"));
						respuesta.setAttribute("sessionIsAdmin", Usuario.getString("isadmin"));
						respuesta.setAttribute("sessionSecureFlag", "");
						respuesta.setAttribute("error",null);
						if(respuesta.getAttribute("sessionIsAdmin").toString().equals("S")){
							response.sendRedirect("menuadmin.jsp");
						}
						if(respuesta.getAttribute("sessionIsAdmin").toString().equals("N")){
							response.sendRedirect("menu.jsp");
						}
					} else {
						DaoUserSec userSec = new DaoUserSec();
						if(userSec.isAcountSec(email, password)){
							respuesta.setAttribute("sessionNombre", userSec.getSecFullName());
							respuesta.setAttribute("sessionEmail", userSec.getSecEmail());
							respuesta.setAttribute("sessionIsAdmin", "S");
							respuesta.setAttribute("sessionSecureFlag", userSec.getSecFlag());
							respuesta.setAttribute("error",null);
							response.sendRedirect("menusecadmin.jsp");
						}else{
							respuesta.setAttribute("error",
									"Usuario y/o contraseņa incorrectos");
						}
					}
					d.desconectar();
					response.sendRedirect("login.jsp");
				} catch (Exception e) {
				}

			} else {
				respuesta.setAttribute("error", "Contraseņa no es valida");
				response.sendRedirect("login.jsp");

			}
		}
		
		

	}
}