package servlets;

import beans.DaoUser;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/DeleteUser")
public class DeleteUser extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7432700501449176526L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("login.jsp");
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession respuesta = request.getSession();
		String userId = request.getParameter("id");
		DaoUser d = new DaoUser();
		if (userId.isEmpty()) {
			respuesta.setAttribute("error", "Se requiere usuario a eliminar");
		} else {
			try {
				d.conectar();
				d.deleteUser(userId);
				d.desconectar();
			} catch (Exception e) {
				respuesta.setAttribute(
						"error",
						"Se ha presentado la sigiente excepcion:"
								+ e.toString());
			}

		}
		response.sendRedirect("usersadmin.jsp");

	}
}