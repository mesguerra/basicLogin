package beans;

import java.sql.*;
import java.util.ArrayList;

public class DaoMenu extends Dao {

	public void registerMenu(String profile, String[] programs)
			throws SQLException {
		for (int i = 0; i < programs.length; i++) {
			String sql = "INSERT INTO `menuProgramas`(`perfilID`,`programaID`) VALUES ('"
					+ profile + "','" + programs[i] + "')";
			PreparedStatement ps = conexion.prepareStatement(sql);
			ps.executeUpdate();
		}
	}

	public void deleteMenu(String perfilID) throws SQLException {
		String sql = "DELETE FROM menuProgramas WHERE perfilID='" + perfilID
				+ "'";
		PreparedStatement ps = conexion.prepareStatement(sql);
		ps.executeUpdate();
	}

	public ResultSet getDataByProfileID(String perfilID) throws SQLException {
		String sql = "SELECT programas.name,programas.description FROM menuProgramas,programas WHERE menuProgramas.perfilID='"
				+ perfilID
				+ "' AND menuProgramas.programaID = programas.programasID";
		PreparedStatement ps = conexion.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			return rs;
		}
		return null;
	}

	public ResultSet getDataByProgramID(String programID) throws SQLException {
		String sql = "SELECT programa.programaID,programa.nombre,programa.descripcion FROM menuProgramas,programas WHERE menuProgramas.programaID='"
				+ programID
				+ "' AND menuProgramas.programaID = programa.programaID";
		PreparedStatement ps = conexion.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			return rs;
		}
		return null;
	}

	public ArrayList<String> getArrayDataByProfileID(String perfilID)
			throws SQLException {
		String sql = "SELECT programaID FROM menuProgramas WHERE perfilID='"
				+ perfilID
				+ "'";
		PreparedStatement ps = conexion.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		ArrayList<String> data = new ArrayList<String>();
		if(rs.next()){
			data.add(rs.getString("programaID"));
		}
		while (rs.next()) {
			data.add(rs.getString("programaID"));
		}
		return data;
	}

	public ArrayList<String> getArraydataByProgramID(String programID)
			throws SQLException {
		String sql = "SELECT perfilID FROM menuProgramas WHERE perfilID='"
				+ programID + "'";
		PreparedStatement ps = conexion.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		ArrayList<String> data = new ArrayList<String>();
		if(rs.next()){
			data.add(rs.getString("perfilID"));
		}
		while (rs.next()) {
			data.add(rs.getString("perfilID"));
		}
		return data;
	}
}