package beans;

import java.sql.*;

public class DaoUser extends Dao {

	public boolean isAcountExists(String email, String password)
			throws SQLException {
		String sql = "SELECT * FROM usuarios WHERE email='" + email
				+ "' AND password='" + password + "'";
		PreparedStatement ps = conexion.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		return rs.next();
	}

	public boolean isEmailRegistered(String email) throws SQLException {
		String sql = "SELECT * FROM usuarios WHERE email='" + email + "'";
		PreparedStatement ps = conexion.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		return rs.next();
	}

	public void registerUser(String email, String password, String name,
			String profile, String isadmin) throws SQLException {
		String sql = "INSERT INTO `usuarios`(`email`,`password`,`name`,`profile`,`isadmin`) VALUES ('"
				+ email
				+ "','"
				+ password
				+ "','"
				+ name
				+ "','"
				+ profile
				+ "','" + isadmin + "')";
		PreparedStatement ps = conexion.prepareStatement(sql);
		ps.executeUpdate();
	}

	public ResultSet getDataByEmail(String email) throws SQLException {
		String sql = "SELECT * FROM usuarios WHERE email='" + email + "'";
		PreparedStatement ps = conexion.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			return rs;
		}
		return null;
	}

	public ResultSet getDataById(String id) throws SQLException {
		String sql = "SELECT * FROM usuarios WHERE usuariosID='" + id + "'";
		PreparedStatement ps = conexion.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			return rs;
		}
		return null;
	}

	public ResultSet getAllData() throws SQLException {
		String sql = "SELECT * FROM usuarios ";
		PreparedStatement ps = conexion.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();

		if (rs.next()) {
			return rs;
		}
		return null;
	}

	public void deleteUser(String id) throws SQLException {
		String sql = "DELETE FROM usuarios WHERE usuariosID='" + id + "'";
		PreparedStatement ps = conexion.prepareStatement(sql);
		ps.executeUpdate();
	}

	public void updateUser(String id, String email, String name,
			String profile, String isadmin, String password) throws SQLException {
		String sql = "UPDATE `usuarios` SET " + "email='" + email + "', "
				+ "name='" + name + "', " + "profile='" + profile + "', "
				+ "password='" + password + "', "
				+ "isadmin='" + isadmin + "' WHERE usuariosID='" + id + "'";
		PreparedStatement ps = conexion.prepareStatement(sql);
		ps.executeUpdate();
	}
}