package beans;
import java.sql.*;
 
public class DaoProgram extends Dao {
	
	
	// Metodo para consultar si un email y contraseñan pertenecen a una cuenta
		// registrada
	public boolean isProgramExists(String name)
			throws SQLException {
		String sql = "SELECT * FROM programas WHERE name='" + name + "'";
		PreparedStatement ps = conexion.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();

		return rs.next();
	}

	

	// Metodo para registrar una cuenta
	public void registerProfile(String name, String description)
			throws SQLException {
		String sql = "INSERT INTO `programas`(`name`,`description`) VALUES ('"
				+ name + "','" + description +"')";
		PreparedStatement ps = conexion.prepareStatement(sql);
		ps.executeUpdate();
	}
    
    public ResultSet getDataByname(String name) throws SQLException{
        String sql = "SELECT * FROM programas WHERE name='"+name+"'";
        PreparedStatement ps = conexion.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        
        if(rs.next()){
            return rs;
        }
        
        return null;
    }
    public ResultSet getAllData() throws SQLException{
        String sql = "SELECT * FROM programas";
        PreparedStatement ps = conexion.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        if(rs.next()){
            return rs;
        }
        return null;
    }
    public void deleteProgram(String id) throws SQLException {
		String sql = "DELETE FROM programas WHERE programasID='" + id + "'";
		PreparedStatement ps = conexion.prepareStatement(sql);
		ps.executeUpdate();
	}
    
    public void updateProgram(String id, String name, String description) throws SQLException {
		String sql = "UPDATE `programas` SET " + "name='" + name + "', "
				+ "description='" + description + "'"
				+ " WHERE programasID='" + id + "'";
		PreparedStatement ps = conexion.prepareStatement(sql);
		ps.executeUpdate();
	}
    public ResultSet getDataById(String id) throws SQLException{
        String sql = "SELECT * FROM programas WHERE programasID='"+id+"'";
        PreparedStatement ps = conexion.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        
        if(rs.next()){
            return rs;
        }
        
        return null;
    }
    
}