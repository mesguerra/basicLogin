package beans;

import java.io.IOException;
import utils.PropertiesReader;

public class DaoUserSec {

	public final static String propFileNameUserSec = "secureuser.properties";
	PropertiesReader readerUser = new PropertiesReader(propFileNameUserSec);

	public boolean isAcountSec(String email, String password) {
		String userSec = "";
		String passSec = "";
		try {
			userSec = readerUser.getPropValues("USRNAME");
			passSec = readerUser.getPropValues("USRPASSWORD");
			if (userSec.equals(email) && passSec.equals(password)) {
				return true;
			}
		} catch (IOException e) {
			System.out.println("IOException: " + e);
		}
		return false;
	}

	public String getSecFullName() {
		try {
			return readerUser.getPropValues("USRFULLNAME");
		} catch (IOException e) {
			System.out.println("IOException: " + e);
		}
		return "";
	}

	public String getSecEmail() {
		try {
			return readerUser.getPropValues("USRNAME");
		} catch (IOException e) {
			System.out.println("IOException: " + e);
		}
		return "";
	}

	public String getSecFlag() {
		try {
			return readerUser.getPropValues("USRSECUREFLAG");
		} catch (IOException e) {
			System.out.println("IOException: " + e);
		}
		return "";
	}

}