package beans;
import java.sql.*;
 
public class DaoProfile extends Dao {
	
	
	public boolean isProfileExists(String name)
			throws SQLException {
		String sql = "SELECT * FROM perfiles WHERE name='" + name + "'";
		PreparedStatement ps = conexion.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();

		return rs.next();
	}

	public void registerProfile(String name, String description)
			throws SQLException {
		String sql = "INSERT INTO `perfiles`(`name`,`description`) VALUES ('"
				+ name + "','" + description +"')";
		PreparedStatement ps = conexion.prepareStatement(sql);
		ps.executeUpdate();
	}
    
    public ResultSet getDataByname(String name) throws SQLException{
        String sql = "SELECT * FROM perfiles WHERE name='"+name+"'";
        PreparedStatement ps = conexion.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        
        if(rs.next()){
            return rs;
        }
        
        return null;
    }
    public ResultSet getDataById(String id) throws SQLException{
        String sql = "SELECT * FROM perfiles WHERE perfilID='"+id+"'";
        PreparedStatement ps = conexion.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        
        if(rs.next()){
            return rs;
        }
        
        return null;
    }
    public ResultSet getAllData() throws SQLException{
        String sql = "SELECT * FROM perfiles";
        PreparedStatement ps = conexion.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        
        if(rs.next()){
            return rs;
        }
        return null;
    }
    public void deleteProfile(String id) throws SQLException {
		String sql = "DELETE FROM perfiles WHERE perfilID='" + id + "'";
		PreparedStatement ps = conexion.prepareStatement(sql);
		ps.executeUpdate();
	}
    
    public void updateProfile(String id, String name, String description) throws SQLException {
		String sql = "UPDATE `perfiles` SET " + "name='" + name + "', "
				+ "description='" + description + "'"
				+ " WHERE perfilID='" + id + "'";
		PreparedStatement ps = conexion.prepareStatement(sql);
		ps.executeUpdate();
	}
}