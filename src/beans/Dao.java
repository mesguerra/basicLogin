package beans;

import java.io.IOException;
import java.sql.*;

import utils.PropertiesReader;

public class Dao {
	public Connection conexion;
	public final static String propFileNameDB = "database.properties";
	PropertiesReader readerDB = new PropertiesReader(propFileNameDB);

	// Conectar a la Base de datos
	public void conectar() {
		try {
			try {
				try {
					Class.forName(readerDB.getPropValues("DBDRIVER"));
					conexion = DriverManager.getConnection(
							readerDB.getPropValues("DBTYPECON") + "://"
									+ readerDB.getPropValues("DBHOST") + ":"
									+ readerDB.getPropValues("DBPORT") + "/"
									+ readerDB.getPropValues("DBNAME") + "",
							readerDB.getPropValues("DBUSER"),
							readerDB.getPropValues("DBPASSWORD"));
				} catch (SQLException sql) {
					System.out.println("SQLException: " + sql);
				}
			} catch (ClassNotFoundException sql) {
				System.out.println("ClassNotFoundException: " + sql);
			}
		} catch (IOException sql) {
			System.out.println("IOException: " + sql);
		}
	}

	// Desconectar a la Base de datos
	public void desconectar() throws SQLException, ClassNotFoundException {
		conexion.close();
	}

}