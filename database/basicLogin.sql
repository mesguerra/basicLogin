/*
SQLyog Community v12.12 (64 bit)
MySQL - 5.7.13-log : Database - menus
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`menus` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `menus`;

/*Table structure for table `menuprogramas` */

DROP TABLE IF EXISTS `menuprogramas`;

CREATE TABLE `menuprogramas` (
  `perfilID` int(11) NOT NULL,
  `programaID` int(11) NOT NULL,
  PRIMARY KEY (`perfilID`,`programaID`),
  KEY `fk_programas_idx` (`programaID`),
  CONSTRAINT `fk_profile2` FOREIGN KEY (`perfilID`) REFERENCES `perfiles` (`perfilID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_programas` FOREIGN KEY (`programaID`) REFERENCES `programas` (`programasID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `menuprogramas` */

LOCK TABLES `menuprogramas` WRITE;

insert  into `menuprogramas`(`perfilID`,`programaID`) values (1,1),(3,1),(1,2),(3,2),(4,2);

UNLOCK TABLES;

/*Table structure for table `perfiles` */

DROP TABLE IF EXISTS `perfiles`;

CREATE TABLE `perfiles` (
  `perfilID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  PRIMARY KEY (`perfilID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `perfiles` */

LOCK TABLES `perfiles` WRITE;

insert  into `perfiles`(`perfilID`,`name`,`description`) values (1,'Ventas','Ventas Institucionales'),(3,'Reportes','Reportes Institucionales'),(4,'Consumo','Sentry');

UNLOCK TABLES;

/*Table structure for table `programas` */

DROP TABLE IF EXISTS `programas`;

CREATE TABLE `programas` (
  `programasID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  PRIMARY KEY (`programasID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `programas` */

LOCK TABLES `programas` WRITE;

insert  into `programas`(`programasID`,`name`,`description`) values (1,'vtap1002','Reporte de Ventas'),(2,'vtap0101a','Ventas POS'),(8,'actp0304','Reporte de Activos');

UNLOCK TABLES;

/*Table structure for table `usuarios` */

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios` (
  `usuariosID` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `profile` int(11) NOT NULL,
  `isadmin` varchar(1) NOT NULL,
  PRIMARY KEY (`usuariosID`),
  KEY `fk_profile_idx` (`profile`),
  CONSTRAINT `fk_profile` FOREIGN KEY (`profile`) REFERENCES `perfiles` (`perfilID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `usuarios` */

LOCK TABLES `usuarios` WRITE;

insert  into `usuarios`(`usuariosID`,`email`,`password`,`name`,`profile`,`isadmin`) values (2,'briajan.acuna@microfocus.com','147852369','Brayan Acuña Reyes',3,'S'),(5,'fernando.calderon@microfocus.com','123456789','Fernando Calderón',1,'N');

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
