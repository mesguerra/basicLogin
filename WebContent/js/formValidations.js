﻿function formElementValidation(elem) {
    //variables globales de validacion
    var resultElementValidation = true;
    //se recupera el tipo del control
    var elementType = elem.tagName.toUpperCase();
	var elementType2 = "";
	console.log(elementType);
    //se recupera si el campo es requerido
    var elementRequired = elem.getAttribute("required");
	console.log(elementRequired);
    var elementFullName = elem.getAttribute("fullname").toUpperCase();
	console.log(elementFullName);
    var elementTextType = null;
    var elementText = null;
    var messajeSend = "";

    var validatePatternExec = null;
    var validatePatternResult = false;
    var validatePattern = false;
    //arreglo de patrones de conincidencia
    var patterns = [
        /^([0-9])*$/, //1 SOLO NUMEROS
        /^([a-zA-ZÑñ])*$/, //2 SOLO LETRAS
        /^([a-zA-ZÑñ0-9])*$/, //3 LETRAS Y NUMERO
        /^([a-zA-ZÑñ ])*$/, //4 LETRAS Y ESPACIO
        /^([a-zA-ZÑñ0-9 ])*$/, //5 LETRAS NUMEROS Y ESPACIO 
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, //6 EMAIL
        /^[a-zA-ZñÑ0-9\-\#]+$/, //7 LETRAS Y NUMEROS CON CARACTERES ESPECIALES SIN ESPACIO.
        /^[a-zA-ZnÑ\-\#]+$/, //8 SOLO LETRAS CON CARACTERES ESPECIALES SIN ESPACIO.
        /^([0-9]{4})[-]([0-9]{2})[-]([0-9]{2})$/, //9 FECHA
        /^[a-zA-ZñÑ0-9 \-\#]+$/, //10 LETRAS Y NUMEROS CON CARACTERES ESPECIALES CON ESPACIO.
        /^[a-zA-ZñÑ \-\#]+$/ //11 SOLO LETRAS CON CARACTERES ESPECIALES CON ESPACIO. 		
    ];

    //determinar el tipo de elemento
    if (elementType == "SELECT") {
        if (elementRequired == "") {
            if (elem.selectedIndex == 0) {
                resultElementValidation = false;
                messajeSend = "DEBE SELECCIONAR UN ELEMENTO DE LA LISTA " + elementFullName;
            }
        }
    } else if (elementType == "CHECKBOX") {
		if(elementRequired == "" && document.getElementByName(elem.name).checked == false){
			resultElementValidation = false;
			messajeSend = "DEBE SELECCIONAR UN ELEMENTO EN " + elementFullName;
		}
    } else if (elementType == "INPUT") {
        elementText = elem.value;
		elementType2 = elem.getAttribute("type").toUpperCase();
        if (elementRequired == "" && elementText.trim() == "") {
            resultElementValidation = false;
            messajeSend = "DEBE INGRESAR TEXTO EN EL CAMPO " + elementFullName;
        } else if (elementRequired == "" && elementText.trim() != "") {
            validatePattern = true;
        } else if (elementRequired != "" && elementText.trim() == "") {
            resultElementValidation = true;
			validatePattern = false;
        } else if (elementRequired != "" && elementText.trim() != "") {
            validatePattern = true;
        } else if (elementType2 == "RADIO") {
			if(elementRequired == "" && document.getElementByName(elem.name).checked == false){
				validatePattern = false;
				validatePatternResult = true;
				resultElementValidation = false;
				messajeSend = "DEBE SELECCIONAR UN ELEMENTO EN " + elementFullName;
			}

		}
        if (validatePattern == true) {
            if (elementType2 == "EMAIL") {
				console.log("EMAIL");
                validatePatternExec = new RegExp(patterns[5]);
                validatePatternResult = validatePatternExec.test(elementText);
            } else if (elementType2 == "DATE") {
				console.log("DATE");
                validatePatternExec = new RegExp(patterns[8]);
                validatePatternResult = validatePatternExec.test(elementText);
            } else if (elementType2 == "TEXT") {
				console.log("TEXT");
				elementTextType = elem.getAttribute("texttype").toUpperCase();
                elementTextType = (elementTextType * 10) / 10;
				console.log(elementTextType);
                switch (elementTextType) {
                    case 1:
                        validatePatternExec = new RegExp(patterns[0]);
                        break;
                    case 2:
                        validatePatternExec = new RegExp(patterns[1]);
                        break;
                    case 3:
                        validatePatternExec = new RegExp(patterns[2]);
                        break;
                    case 4:
                        validatePatternExec = new RegExp(patterns[3]);
                        break;
                    case 5:
                        validatePatternExec = new RegExp(patterns[4]);
                        break;
                    case 6:
                        validatePatternExec = new RegExp(patterns[5]);
                        break;
                    case 7:
                        validatePatternExec = new RegExp(patterns[6]);
                        break;
                    case 8:
                        validatePatternExec = new RegExp(patterns[7]);
                        break;
                    case 9:
                        validatePatternExec = new RegExp(patterns[8]);
                        break;
					case 10:
                        validatePatternExec = new RegExp(patterns[9]);
                        break;
					case 11:
                        validatePatternExec = new RegExp(patterns[10]);
                        break;
                }
                validatePatternResult = validatePatternExec.test(elementText);

            } else if (elementType2 == "NUMBER") {
				console.log("NUMBER");
                validatePatternExec = new RegExp(patterns[0]);
                validatePatternResult = validatePatternExec.test(elementText);

            }
            if (validatePatternResult == false) {
                resultElementValidation = validatePatternResult;
                messajeSend = "FORMATO INVALIDO EN EL CAMPO " + elementFullName;
            }
        }
    }
    //si validaciones es false, despliega mensaje
    if (resultElementValidation == false) {
        //elem.focus();
		pintaMessaje("01", messajeSend);
    } else {
        //document.getElementById(elem.id + 1).focus();
    }
}