//console.log("Hola functions");

var ultimoEnter = new Date();

/*
 * $( document ).ready(function() { ponerCurrentDate(); setInterval(function(){
 * ponerCurrentDate(); },40000);
 * 
 * });
 */

function anadirEnter(ident, callback) {
	// //console.log("anadir Enter");
	$(ident).bind("keypress", function(e) {
		// console.log("Presiono "+e.keyCode);
		if (e.keyCode == 13) {
			var tiempo = segundosDesde(ultimoEnter, new Date());
			console.log("ha pasado " + tiempo + " segundos");
			if (tiempo > 1) {
				console.log("Presiono enter en " + ident);
				ultimoEnter = new Date();
				callback();
			}
			return false;
		}
	});
}

function ponerCurrentDate() {
	var d = new Date();
	var fecha = d.getFullYear() + "/" + (d.getMonth() + 1) + "/" + d.getDate();
	ponerValorLabel(fecha, "#fechaDinamica");
	// console.log("Fecha: "+fecha);
	var horas = d.getHours();
	if (horas > 12) {
		horas = horas - 12;
		var pm = "pm";
	} else {
		var pm = "am";
	}
	var minutes = d.getMinutes();
	if (minutes < 10) {
		minutes = "0" + minutes.toString();
	}
	var tiempo = horas + ":" + minutes + " " + pm;
	// //console.log("Hora: "+tiempo);
	ponerValorLabel(tiempo, "#horaDinamica");
}
function segundosDesde(fecha1, fecha2) {
	return (fecha2 - fecha1) / 1000;
}
function validar() {
	// //console.log("function");
	return false;
}
function preparar(arre) {
	// //console.log(arre);
	var arregloValores = [];
	for (var i = 0; i < arre.length; i++) {
		if (arre[i].indexOf("#") < 0) {
			arregloValores.push(arre[i]);
		} else {
			if (!$(arre[i]).length) {
				// console.log("no existe "+arre[i]);
			} else {
				// console.log("existe "+arre[i]);

				console.log("Pon: " + arre[i]);
				if ($(arre[i]).attr('type') == "text"
						|| $(arre[i]).attr('type') == "hidden"
						|| $(arre[i]).attr('class') == "desplegable"
						|| $(arre[i]).attr('class') == "label"
						|| $(arre[i]).attr('type') == "email") {
					if ($(arre[i]).attr('type') == "text"
							&& $(arre[i]).val().length == 0) {
						var res = $(arre[i]).data("egogc-defecto");
						console.log("Defecto: " + res + " para id " + arre[i]);
						if (res != undefined) {
							console.log("Defecto: " + res + " para id "
									+ arre[i]);
							arregloValores.push(res);
						} else {
							arregloValores.push($(arre[i]).val());
						}

					} else {
						var ide = arre[i];
						arregloValores.push($.trim($(ide).val()));
					}
				} else if ($(arre[i]).attr('type') == "date") {
					var ide = arre[i];
					console.log("Fecha es: " + $(ide).val());
					var strin = $(ide).val();
					for (var j = 0; j < strin.length; j++) {
						strin = strin.replace("-", "");
					}
					arregloValores.push(strin);
				}/*
					 * else if($(arre[i]).children().get(0).type == "radio"){
					 * var nombre = $(arre[i]).children().get(0).name;
					 * arregloValores.push(retornaRadioValue(nombre)); }
					 */else {

				}
			}
		}
	}
	dat = "||" + arregloValores.join("|");
	dat += '|';
	// //console.log("la trama hasta aca: "+dat);
	return dat;
}
function retornaValor(ide) {
	if ($(ide).attr('type') == "text" || $(ide).attr('class') == "desplegable") {
		if ($(ide).attr('type') == "text" && $(ide).val().length == 0) {
			var res = $(ide).data("egogc-defecto");
			console.log("Defecto: " + res + " para id " + ide);
			return res;
		} else {
			return $(ide).val();
		}
	} else if ($(ide).attr('type') == "date") {
		// console.log("Fecha es: "+$(ide).val());
		return $(ide).val().replace("-", "");
	} else if ($(ide).children().get(0).type == "radio") {
		var nombre = $(ide).children().get(0).name;
		return retornaRadioValue(nombre);
	} else {

	}
}
function retornaRadioValue(name) {
	var cadena = "input[name=" + name + "]:checked";
	return $(cadena).val();
}
function enviar(arch, tra, ope, iden) {
	// //console.log("hola");
	// var ruta="../servicio.php";
	// var ruta ="http://localhost:8080/JSPProyect/recibeDatos";
	var urlLlamados = location.protocol + "//" + location.hostname;
	var ruta = urlLlamados + ":8080/JSPProyect6/recibeDatos";
	// "http://localhost:8080/JspProject/recibeDatos
	// ?archivo='espno003'&trama='2015|32|0|0|/*'&operacion='1'";
	// http://localhost:8080/JspProject/espno003/indexespno003.htm

	console.log("En enviar la trama: " + tra);
	$.get(ruta, {
		archivo : arch,
		trama : tra,
		operacion : ope
	}, function(data, status) {
		// //console.log("Recibi desde get: "+JSON.stringify(data)+" el status
		// "+status);
		var json = JSON.parse(data);
		var jso = json.mensaje;// .replace("/","");
		jso = jso.replace("*", "");
		jso = jso.replace(" ", "");
		jso = jso.substring(1);
		var arre = jso.split("|");
		for (var i; i < arre.length; i++) {
			arre[i] = arre[i].replace("*", "");
			arre[i] = arre[i].replace("/", "");
			arre[i] = arre[i].replace("|", "");
		}
		arre.splice(arre.length - 1, 1);
		resultado(arre, iden);
	});
}
function limpiarDatos(key) {
	sessionStorage.removeItem(key);
}
function guardarDatos(key, datos) {
	sessionStorage.setItem(key, JSON.stringify(datos));
}
function cargarDatos(key) {
	var datass = sessionStorage.getItem(key);
	// //console.log("data: "+datass);
	limpiarDatos(key);
	return JSON.parse(datass);
}
function estaGuardada(key) {
	var datass = sessionStorage.getItem(key);
	if (datass) {
		return true;
	} else {
		return false;
	}
}
function navegarA(pagina) {
	window.location.href = pagina;
}
function stringFecha(cadena) {
	if (cadena.length != 8) {
		// //console.log(cadena+" no es una cadena valida para fecha");
	} else {
		var ano = cadena.substring(0, 4);
		var mes = cadena.substring(4, 6);
		var dia = cadena.substring(6);
		var fecha = ano + "-" + mes + "-" + dia;
		// //console.log("cadena quedaria: "+fecha);
		return fecha;
	}
}
function ponerValor(valor, ident) {

	if (valor == "0000000000" || valor == "0000000" || valor == "00000000") {
		valor = "0";
	}
	// console.log("ident poner "+ident+ " el valor: "+valor);
	var la = ident;
	if (!$(la).length) {
		// console.log("No puse "+ident+", no viene valor")
		return;
	}
	if (ident == "#id6") {
		// //console.log("Llego 6");
	}

	if ($(la).attr('type') == "text" || $(la).attr('type') == "email"
			|| $(la).attr('class') == "desplegable") {
		ponerValorTextfield(valor, ident);
	} else if ($(la).attr('type') == "date") {
		ponerValorFecha(valor, ident);
	} else if ($(la).attr('class') == "label") {
		ponerValorLabel(valor, ident);
	} else if ($(la).children().get(0).type == "radio") {
		ponerValorRadio(valor, ident);
	} else {
		// //console.log("No pude poner: "+ident);
	}
}
function ponerValorLabel(valor, label) {
	var la = label;
	$(la).text(valor);
}
function ponerValorTextfield(valor, textfield) {
	var la = textfield;
	$(la).val(valor);
}
function ponerValorRadio(valor, radio) {
	if (valor == "S" || valor == "N" || valor == "1" || valor == "0"
			|| valor == "F" || valor == "M") {
		var nombre = $(radio).children().get(0).name;
		$("input[name=" + nombre + "][value=" + valor + "]").attr('checked',
				'checked');
		$("input[name=" + nombre + "][value=" + valor + "]").prop('checked',
				true);
	} else {
		// console.log("El valor:"+valor+" no es valido para radio "+radio);
	}
}
function ponerValorFecha(valor, fecha) {
	var la = fecha;
	var ano = valor.substring(0, 4);
	var mes = valor.substring(4, 6);
	var dia = valor.substring(6);
	// yyyy-MM-dd
	// var cadena = dia+"-"+mes+"-"+ano;
	var cadena = ano + "-" + mes + "-" + dia;
	$(la).val(cadena);
}
function validarCampo(ident) {
	var temp = $(ident).val();
	if (temp.length > 0) {
		return true;
	} else {
		return false;
	}
}
function existeDato(cadena, arreglo) {
	for (var i = 0; i < arreglo.length; i++) {
		if (arreglo[i].indexOf(cadena) >= 0) {
			console.log(i + "-" + arreglo[i] + "EXISTE");
			return true;
		}
	}
	console.log(i + "-" + arreglo[i] + "NO EXISTE");
	return false;
}
function estaValidadoElForm(ide) {
	if ($(ide)[0].checkValidity()) {
		return true;
	} else {
		return false;
	}
}

function ponerValorFechaLabel(valor, fecha) {

	var la = fecha;
	var ano = valor.substring(0, 4);
	var mes = valor.substring(4, 6);
	var dia = valor.substring(6);
	// yyyy-MM-dd
	var cadena = dia + "/" + mes + "/" + ano;
	console.log("año " + ano);
	console.log("mes " + mes);
	console.log("dia" + dia);
	console.log("caja " + fecha);
	$(la).val(cadena);

}

function validaRangoFechas(inicialp, finallp) {

	var inicial = document.getElementById("id" + inicialp).value.replace(/-/g,
			"");
	var finall = document.getElementById("id" + finallp).value
			.replace(/-/g, "");
	if (Number(finall) >= Number(inicial)) {
		return true;
	} else {
		document.getElementById("id" + finallp).focus();
		return false;
	}
}

function validaRango(inicialp, finallp) {

	var inicial = document.getElementById("id" + inicialp).value;
	var finall = document.getElementById("id" + finallp).value;
	if (Number(finall) >= Number(inicial)) {
		return true;
	} else {
		document.getElementById("id" + finallp).focus();
		return false;
	}
}

$(document).keypress(function(event) {
	if (event.which == '13') {
		event.preventDefault();
		return false;
	}
});
