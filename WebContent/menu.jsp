<%@taglib prefix="t" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="beans.*"%>
<%@page import="java.sql.ResultSet"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%-- En caso de que exista una sesion iniciada redirecciono a index.jsp. "NO tiene caso mostrar este formulario cuando hay una sesion iniciada --%>
<t:if test="${sessionScope['sessionEmail']==null}">
	<%
		response.sendRedirect("login.jsp");
	%>
</t:if>
<!DOCTYPE html>


<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Home Sentry</title>

<link href="menu_estilo/css/bootstrap.min.css" rel="stylesheet">
<link href="menu_estilo/css/datepicker3.css" rel="stylesheet">
<link href="menu_estilo/css/styles.css" rel="stylesheet">
<link rel="icon" href="images/hsentrylogo.ico" type="image/x-icon">

<meta charset="utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
<link rel="stylesheet" href="css/normalize.css">
<link rel="stylesheet" href="css/lightbox.css">
<link rel="stylesheet" href="css/boto.css">
<link rel="stylesheet" href="css/tooltip.css">
<link rel="shortcut icon" type="image/png"
	href="http://cdn.totalcode.com/homesentry/images/banners/favicon.png" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700"
	rel="stylesheet">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<script src="js/validar.js"></script>


<link href="themes/1/js-image-slider.css" rel="stylesheet"
	type="text/css" />
<script src="themes/1/js-image-slider.js" type="text/javascript"></script>
<link href="generic.css" rel="stylesheet" type="text/css" />

<!--Icons-->
<script src="menu_estilo/js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
			<script src="menu_estilo/js/html5shiv.js"></script>
			<script src="menu_estilo/js/respond.min.js"></script>
		<![endif]-->


</head>

<body>

	<div class="textHeader">
		<div class="contenidoTextHeader">
			<div class="contenedorLogo">
				<div class="logoFoto"></div>
			</div>
			<div class="columText">
				<div class="filaBlanca">
					<p class="bienvenido" style="font-size: 19.2px !important;">
						<strong>Bienvenidos</strong> - Home Sentry Calle 127
					</p>
				</div>
				<div class="filaGris">
					<p class="nomFactura" style="font-size: 19.2px !important;">
						<strong>Men&uacute; Principal</strong>
					</p>
				</div>
				<div class="filaBlanca">
					<p class="nrofacturaa" style="font-size: 19.2px !important;">
						<strong> </strong>
					</p>
				</div>
			</div>
			<div class="columText">
				<div class="filaBlanca">
					<p class="anoFac" name="ano" style="font-size: 19.2px !important;">
						<strong>A&ntilde;o: </strong><strong id="anioActualx">
							WXYZ</strong>
					</p>
				</div>
				<div class="filaGris">
					<p class="atendido" style="font-size: 19.2px !important;">
						<strong>Usuario: </strong>
						${sessionScope['sessionNombre'].toUpperCase()}
					</p>
				</div>
				<div class="filaBlanca">
					<p class="fechaaa" id="fechaDinamica"
						style="font-size: 20.8px !important;">
						<%
							Date dNow = new Date();
							SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
							String currentDate = ft.format(dNow);
							out.println(currentDate);
						%>
					</p>

					<p class="hora" style="font-size: 20.8px !important;">
						<%
							SimpleDateFormat ft2 = new SimpleDateFormat("hh:mm aa");
							currentDate = ft2.format(dNow);
							out.println(currentDate);
						%>
					</p>
					<!-- <label class="hora" id="horaDinamica"></label> -->
				</div>
			</div>
		</div>
	</div>
	<hr>
	<!-- <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container-fluid">
			<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href=""><span><left><img href="" src="../images/hsentrylogo.png"/></left> Home</span>Sentry</a>
			<ul class="user-menu">
			<li class="dropdown pull-right">
			<a class="dropdown-toggle" data-toggle="dropdown" color="white"> Usuario: Auditoria </a>
			</li>
			</ul>
			</div>
			
			</div>
		</nav>-->

	<div id="sidebar-collapse" class="col-sm-2 col-lg-3 sidebar"
		style="margin-top: 82px; width: 370px !important; box-shadow: 0px 0px 5px rgba(0, 0, 0, 0);">
		<ul class="nav menu">
			<li role="presentation" class="divider"></li>
			<li><a href="" class="tipoFuente"><svg
						class="glyph stroked star">
						<use xlink:href="#stroked-star"></use></svg> Home</a></li>

			<li class="parent "><a href="#"> <span
					data-toggle="collapse" href="#sub-item-2"><svg
							class="glyph stroked chevron-down tipoFuente">
							<use xlink:href="#stroked-chevron-down"></use></svg></span> <b
					class="tipoFuente">Men&uacute; 
					
					<%
						DaoProfile profile = new DaoProfile();
						profile.conectar();
						ResultSet rs3 = profile.getDataById(session.getAttribute(
								"sessionProfile").toString());
						out.write(rs3.getString("name"));
						profile.desconectar();
					%>
					
					</b>
			</a>
				<ul class="children collapse" id="sub-item-2">
					<li style="height: 10px;"><a class="tipoFuente"
						id="linkprogram" href="#"> <svg
								class="glyph stroked chevron-right">
								<use xlink:href=""></use></svg>
					</a></li>

					<%
						DaoMenu menu = new DaoMenu();
						menu.conectar();
						ResultSet rs2 = menu.getDataByProfileID(session.getAttribute(
								"sessionProfile").toString());
					%>
					<li><a class="tipoFuente"
						href="<%=rs2.getString("programas.name")%>/index.jsp"> <svg
								class="glyph stroked chevron-right">
								<use xlink:href="#stroked-chevron-right"></use></svg> <%=rs2.getString("programas.name") + " "
					+ rs2.getString("programas.description")%>
					</a></li>
					<%
						while (rs2.next()) {
					%>
					<li><a class="tipoFuente"
						href="<%=rs2.getString("programas.name")%>/index.jsp"> <svg
								class="glyph stroked chevron-right">
								<use xlink:href="#stroked-chevron-right"></use></svg> <%=rs2.getString("programas.name") + " "
						+ rs2.getString("programas.description")%>
					</a></li>
					<%
						}
						menu.desconectar();
					%>




				</ul></li>



			<li><a href="LogoutUser" class="tipoFuente"><svg
						class="glyph stroked cancel">
						<use xlink:href="#stroked-cancel"></use></svg> Cerrar Sesi&oacute;n</a></li>

		</ul>

	</div>
	<!--/.sidebar-->
	<section>
		<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main"
			id="pantalla">
			<div class="row"></div>
		</div>
	</section>
	<script src="menu_estilo/js/jquery-1.11.1.min.js"></script>
	<script src="menu_estilo/js/bootstrap.min.js"></script>

	<!--<script src="menu_estilo/js/easypiechart.js"></script>-->
	<!--<script src="menu_estilo/js/easypiechart-data.js"></script>-->
	<!--<script src="menu_estilo/js/bootstrap-datepicker.js"></script>-->
	<!--<script>
				$('#calendar').datepicker({
				});
				
				!function ($) {
				$(document).on("click","ul.nav li.parent > a > span.icon", function(){          
				$(this).find('em:first').toggleClass("glyphicon-minus");      
				}); 
				$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
				}(window.jQuery);
				
				$(window).on('resize', function () {
				if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
				})
				$(window).on('resize', function () {
				if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
				})
			</script>	-->
	<script type="text/javascript">
		$(document).ready(function() {
			$("#linkprogram").click(function(e) {
				e.preventDefault();
				$("#someFrame").attr("src", $(this).attr("href"));

			});
			$("button").click(function(e) {
				e.preventDefault();
				$("#someFrame").removeAttr("src");

			});
		});
	</script>
	<script>
		//alert("window.screen.availWidth" + window.screen.availWidth);
		if (window.screen.availWidth <= 1366) {
			document.body.style.zoom = "90%";

		}
		$(function() {

			var $body = $(document);
			$body.bind('scroll', function() {
				// "Desactivar" el scroll horizontal
				if ($body.scrollLeft() !== 0) {
					$body.scrollLeft(0);
				}
			});

		});
	</script>
</body>
<script>
	var currentTime = new Date();
	var year = currentTime.getFullYear();
	document.getElementById("anioActualx").innerHTML = year;
</script>
</html>

<%
	session.setAttribute("error", "");
%>