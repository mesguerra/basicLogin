       identification division.
           program-id.    idxtbllog.
           author.        Briajan Acu�a.
           security.      Copyright Gladius Technologies SAS 2015.

       environment division.
       configuration section.

       select pdtbllog assign pdf-tbl-log
           organization line sequential
           status                 is file-stat.

       select idxtbllog assign idx-tbl-log
           organization           is indexed
           access mode            is dynamic
           record key             is nom-csv
                                     with duplicates 
           status                 is file-stat.

       file section.

       fd pdtbllog.
       01 reg-tablas-log.
          02 nom-fd                    pic x(50).
          02 a1                        pic x.
          02 nom-tabla                 pic x(50).
          02 a2                        pic x.
          02 nom-sub-tabla             pic x(50).
          02 a3                        pic x.
          02 descrip-crea              pic x(50).
          02 a4                        pic x.
          02 descrip-tbl               pic x(80).  

       fd idxtbllog.
       01 idxtbllog1.
          02 nom-csv                   pic x(50).
          02 nom-tbl                   pic x(50).
          02 nom-sub-tbl               pic x(50).
          02 des-crea                  pic x(50).
          02 des-tbl                   pic x(80).

       working-storage section.
      ******************************************************************
      * file status
      ******************************************************************
       01  file-stat.
          05 status-key-1              pic x.
          05 status-key-2              pic x.
          05 status-key-2-binary redefines status-key-2 
                                       pic 99 comp-x.    
      ******************************************************************
      *variables
      ******************************************************************
       01 variables.
          05 sw-ok                     pic x value spaces.
          
      *swhitches
       01 swhitches.
          05 sw-pdtbllog               pic 9 value 0.
          05 sw-idxtbllog              pic 9 value 0.
          
      ******************************************************************                                                                *
       Procedure Division.
      ******************************************************************
       inicio-proceso section.
           display "inicia ok"
           perform 1000-inicio
           perform 2000-proceso until sw-pdtbllog = 1
           perform 3000-fin.
       
       1000-inicio section.
           initialize variables swhitches
           move 0                      to sw-pdtbllog
           move  "C:\BAARVC\pdtbllog"
                                       to pdf-tbl-log
           move  "C:\BAARVC\idxtbllog"
                                       to idx-tbl-log
           perform 1010-open-archivos.
           
       1010-open-archivos section.
           open input  pdtbllog
           open output idxtbllog
           close       idxtbllog
           open i-o    idxtbllog.
           
       2000-proceso section.
           read pdtbllog next
              at end
                 move 1                to sw-pdtbllog
           end-read
           
           if sw-pdtbllog = 0
              perform 2010-mover-txt-idx
              perform 2020-write-idx
           end-if.
           
       2010-mover-txt-idx section.
           initialize idxtbllog1
           move nom-fd                 to nom-csv     
           move nom-tabla              to nom-tbl    
           move nom-sub-tabla          to nom-sub-tbl
           move descrip-crea           to des-crea   
           move descrip-tbl            to des-tbl.
        
       2020-write-idx section.
           write idxtbllog1
              invalid key
                 move 1                to sw-pdtbllog
           end-write.      
           
       3000-fin section.
           close pdtbllog idxtbllog
           goback.