      $set mfoo optional-file
      *>-----------------------------------------------------------
      *> ejeprogram = ejecuta prgramas llamados desde JSP   
      *>-----------------------------------------------------------
       class-id. ejeprogram public.

       method-id. "Pasa-Parm".
	   
	   working-storage section.
	   01 ws-parametros.
	      02 ws-programa               pic x(20)   value spaces.
          02 ws-parm-res               pic x(9980) value spaces. 		  

       local-storage section.
       linkage section.
       01  lnk-parametros              pic x(1000).
	   
       procedure division returning lnk-parametros.
           move lnk-parametros         to ws-parametros   
           call "ws-programa" using ws-parm-res  
           cancel "ws-programa" 
		   goback.

       end method.       
       end class.