<%@page import="org.apache.commons.lang3.ArrayUtils"%>
<%@taglib prefix="t" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="beans.*"%>
<%@page import="java.sql.ResultSet"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.apache.commons.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%-- En caso de que exista una sesion iniciada redirecciono a index.jsp. "NO tiene caso mostrar este formulario cuando hay una sesion iniciada --%>
<t:if test="${sessionScope['sessionEmail']==null}">
	<%
		response.sendRedirect("login.jsp");
	%>
</t:if>
<t:if test="${sessionScope['sessionIsAdmin']=='N'}">
	<%
	session.setAttribute("error", "Su usuario esta intentando ingresar a un perfil no autorizado");
	response.sendRedirect("login.jsp");
	%>
</t:if>
<html class="" lang="es-ES">
<head>
<title>Home Sentry</title>
<meta name="description" content="Aca va la descripci&oacute;n" />
<meta charset="ISO-8859-1" />
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
<link rel="stylesheet" href="css/normalize.css">
<link rel="stylesheet" href="css/lightbox.css">
<link rel="stylesheet" href="css/boto.css">
<link rel="stylesheet" href="css/tooltip.css">
<link rel="shortcut icon" type="image/png"
	href="http://cdn.totalcode.com/homesentry/images/banners/favicon.png" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700"
	rel="stylesheet">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<script src="js/validar.js"></script>
<link href="https://fonts.googleapis.com/css?family=Oswald"
	rel="stylesheet">
<script src="js/funciones.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.10.2/validator.min.js"></script>
</head>
<body style="overflow: none !important;">
	<!--EMPIEZA EL WRAPPER GENERAL -->
	<div id="wrapperGeneral">
		<section class="columnaPrincipal">
			<article class="textInformativo">
				<!--EMPIEZA EL HEADER DEL TEXTO AL 100% -->
				<div class="textHeader">
					<div class="contenidoTextHeader">
						<div class="contenedorLogo">
							<div class="logoFoto"></div>
						</div>
						<div class="columText">
							<div class="filaBlanca">
								<p class="bienvenido">
									<strong>Bienvenidos</strong> - Home Sentry Calle 127
								</p>
							</div>
							<div class="filaGris">
								<p class="nomFactura">
									<strong>Administraci&oacute;n</strong>
								</p>
							</div>
							<div class="filaBlanca">
								<p class="nrofacturaa">
									<strong>Administrar Menu de Perfil</strong>
								</p>
							</div>
						</div>
						<div class="columText">
							<div class="filaBlanca">
								<p class="anoFac" name="ano">
									<strong>A&ntilde;o: </strong><strong id="anioActualx">
										WXYZ</strong>
								</p>
							</div>
							<div class="filaGris">
								<p class="atendido">
									<strong>Usuario: </strong>
									${sessionScope['sessionNombre'].toUpperCase()}
								</p>
							</div>
							<div class="filaBlanca">
								<p class="fechaaa" id="fechaDinamica"
									style="font-family: 'Source Sans Pro', sans-serif;">
									<%
										Date dNow = new Date();
										SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
										String currentDate = ft.format(dNow);
										out.println(currentDate);
									%>
								</p>

								<p class="hora"
									style="font-family: 'Source Sans Pro', sans-serif;">
									<%
										SimpleDateFormat ft2 = new SimpleDateFormat("hh:mm aa");
										currentDate = ft2.format(dNow);
										out.println(currentDate);
										SimpleDateFormat ft3 = new SimpleDateFormat("yyyy");
										currentDate = ft3.format(dNow);
									%>
								</p>


							</div>
						</div>
					</div>
				</div>
				<!--TERMINA EL HEADER DEL TEXTO AL 100% -->
				<hr>
				<!--EMPIEZA EL HEADER 2 -->

				<form action="RegisterMenu" method="post">

					<div class="textHeaderDosGrande" style="height: 100% !important;">
						<div class="contentHeaderDos">
							<div class="columnasTresContenidoCentrado">
								<div class="textFilaContenido">
									<div class="parrafofilaUno">
										<p style="color: #ff0000">${sessionScope['error']}</p>
									</div>
									<p class="parrafofilaDos">&nbsp;</p>
								</div>
								<div class="textFilaContenido">
									<div class="parrafofilaUno">Perfil:</div>
									<div class="parrafofilaDos">

										<select name="profile" class="desplegable desplegableSucursales">
											<%
												DaoProfile profile = new DaoProfile();
												profile.conectar();
												ResultSet rs = profile.getDataById(session.getAttribute("id")
							.toString());
											%>
											<option value="<%=rs.getString("perfilID")%>" 
											<%if (rs.getString("perfilID").equals(session.getAttribute("id")
													.toString())) {%>
						 						selected
											<%} %>
											>
												<%=rs.getString("name") + " - "
					+ rs.getString("description")%>
											</option>
											<%
												while (rs.next()) {
											%>
											<option value="<%=rs.getString("perfilID")%>" 
											<%if (rs.getString("profile").equals(rs.getString("perfilID"))) {%>
						 						selected
											<%} %>
											>
											<%=rs.getString("name") + " - "
					+ rs.getString("description")%>
											</option>
											<%
												}
											%>
										</select> <label class="labelObligatorio">*</label>

									</div>
								</div>
								<div class="textFilaContenido">
									<p class="parrafofilaUno">Programas:</p>
									<p class="parrafofilaDos">
										<select name="programs" multiple="multiple" class="desplegable desplegableSucursales">
				<%
				
					DaoMenu menu = new  DaoMenu();
					menu.conectar();
					ArrayList<String> listMenu = menu.getArrayDataByProfileID(session.getAttribute("id")
							.toString());
					menu.desconectar();
					String[] listMenuSearch = new String[listMenu.size()];
					for(int i = 0; i <listMenu.size();i++ ){
						listMenuSearch[i]=listMenu.get(i);
					}
					
					
					DaoProgram program = new DaoProgram();
					program.conectar();
					ResultSet rs2 = program.getAllData();
					
					
					
					out.write("<option value ='" + rs2.getString("programasID") + "'");
					if(ArrayUtils.contains(listMenuSearch, rs2.getString("programasID"))){
						out.write(" selected ");
					}
					out.write(">"
							+ rs2.getString("name") + " - "
							+ rs2.getString("description") + "</option>");
					while (rs2.next()) {
						out.write("<option value ='" + rs2.getString("programasID") + "'");
						//ArrayUtils utils = new ArrayUtils();
						if(ArrayUtils.contains(listMenuSearch, rs2.getString("programasID"))){
							out.write(" selected ");
						}
						out.write(">"
								+ rs2.getString("name") + " - "
								+ rs2.getString("description") + "</option>");
					}
					program.desconectar();
					profile.desconectar();
				%>
			</select>
									</p>
								</div>
								<div class="botones">
									<button type="submit" class="botonPequeno" id="botonEspacio" 
									style="height: 39px;width: 120px;">
										<i class="fa fa-cogs" aria-hidden="true"></i>
									</button>
									<a href="profilesadmin.jsp" class="botonPequeno" id="botonEspacio"
										style="background-color: #eb1c24; text-decoration: none;">
										<i class="" aria-hidden="true">Regresar</i>
									</a>
								</div>

							</div>
						</div>

					</div>
					<!--TERMINA EL CONTENIDO AL 100% -->

					<!--EMPIEZA LOS BOTONES 100% -->
				</form>
				<!--COMIENZA LIGHTBOX -->
				<!--TERMINA LIGHTBOX -->
			</article>
			<div class="footer"></div>
		</section>
	</div>
	<!--ACABA EL WRAPPER GENERAL -->
	<script src=""></script>
	<script src="js/pestanias.js"></script>
	<script src="js/readmore.js"></script>



	<script>
		if (window.screen.availWidth <= 1366) {
			document.body.style.zoom = "90%";
		}
		$(function() {
			var $body = $(document);
			$body.bind('scroll', function() {
				if ($body.scrollLeft() !== 0) {
					$body.scrollLeft(0);
				}
			});

		});
	</script>
</body>
<script>
	var currentTime = new Date();
	var year = currentTime.getFullYear();
	document.getElementById("anioActualx").innerHTML = year;
</script>
</html>
<%
	session.setAttribute("error", "");
%>