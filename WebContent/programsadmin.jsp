<%@taglib prefix="t" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="beans.*"%>
<%@page import="java.sql.ResultSet"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%-- En caso de que exista una sesion iniciada redirecciono a index.jsp. "NO tiene caso mostrar este formulario cuando hay una sesion iniciada --%>
<t:if test="${sessionScope['sessionEmail']==null}">
	<%
		response.sendRedirect("login.jsp");
	%>
</t:if>
<t:if test="${sessionScope['sessionIsAdmin']=='N'}">
	<%
	session.setAttribute("error", "Su usuario esta intentando ingresar a un perfil no autorizado");	
	response.sendRedirect("login.jsp");
	%>
</t:if>
<!DOCTYPE HTML>
<html class="" lang="es-ES">
<head>
<title>Home Sentry</title>
<meta name="description" content="Aca va la descripci�n" />
<meta charset="utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
<link rel="stylesheet" href="css/normalize.css">
<link rel="stylesheet" href="css/lightbox.css">
<link rel="stylesheet" href="css/boto.css">
<link rel="shortcut icon" type="image/png"
	href="http://cdn.totalcode.com/homesentry/images/banners/favicon.png" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700"
	rel="stylesheet">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<script src="js/validar.js"></script>
</head>
<body>

	<!--EMPIEZA EL WRAPPER GENERAL -->
	<div id="wrapperGeneral">
		<section class="columnaPrincipal">
			<article class="textInformativo">
				<!--EMPIEZA EL HEADER DEL TEXTO AL 100% -->
				<div class="textHeader">
					<div class="contenidoTextHeader">
						<div class="contenedorLogo">
							<div class="logoFoto"></div>
						</div>
						<div class="columText">
							<div class="filaBlanca">
								<p class="bienvenido">
									<strong>Bienvenidos</strong> - Home Sentry Calle 127
								</p>
							</div>
							<div class="filaGris">
								<p class="nomFactura">
									<strong>Administraci&oacute;n</strong>
								</p>
							</div>
							<div class="filaBlanca">
								<p class="nrofacturaa">
									<strong></strong>
								</p>
							</div>
						</div>
						<div class="columText">
							<div class="filaBlanca">
								<p class="anoFac" name="ano">
									<strong>A&ntilde;o: </strong><strong id="anioActualx">
										WXYZ</strong>
								</p>
							</div>
							<div class="filaGris">
								<p class="atendido">
									<strong>Usuario: </strong>
									${sessionScope['sessionNombre'].toUpperCase()}
								</p>
							</div>
							<div class="filaBlanca">
								<p class="fechaaa" id="fechaDinamica"
									style="font-size: 20.8px !important;">
									<%
										Date dNow = new Date();
										SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
										String currentDate = ft.format(dNow);
										out.println(currentDate);
									%>
								</p>

								<p class="hora" style="font-size: 20.8px !important;">
									<%
										SimpleDateFormat ft2 = new SimpleDateFormat("hh:mm aa");
										currentDate = ft2.format(dNow);
										out.println(currentDate);
									%>
								</p>
								<!-- <label class="hora" id="horaDinamica"></label> -->
							</div>
						</div>
					</div>
				</div>
				<!--TERMINA EL HEADER DEL TEXTO AL 100% -->
				<hr>
				<!--EMPIEZA EL HEADER 2 -->
				<!--BOTONES -->

				<!--BOTONES -->
				<div class="textHeaderDos" style="padding-top: 40px;"></div>
				<!--TERMINA EL HEADER 2 -->
				<!--EMPIEZA EL CONTENIDO AL 100% -->
				<div class="contenidoDos">
					<div style="padding-top: -90px;margin-left: 20px;">
						<%
							if (session.getAttribute("sessionSecureFlag").equals("Y")) {
						%>
						<a href="menusecadmin.jsp" class="botonPequeno"
							style="background-color: #eb1c24; text-decoration: none;"> <i
							class="" aria-hidden="true">Regresar</i>

						</a>
						<%
							} else {
						%>
						<a href="menuadmin.jsp" class="botonPequeno"
							style="background-color: #eb1c24; text-decoration: none;"> <i
							class="" aria-hidden="true">Regresar</i>

						</a>
						<%
							}
						%>&nbsp;  <a href="registerprogram.jsp" class="botonPequeno" style="text-decoration:none;"> <i
							class="" aria-hidden="true">Crear Programa</i>
						</a>
					</div>
					<br>
					<div class="textFilaContenidoDos">
						<br>
						<!--EMPIEZAN LAS PESTA�AS -->
						<div id="responsiveTabsDemo">
							<ul>
								<li><a href="#tab-1"> Programas </a></li>
							</ul>
							<div id="tab-1">
								<br>
								<div class="contenidoTabs">
									<div class="datosReferencias">
										<div class="filaTitularesDescriptivosCuatro" id="bgcolor">
											<p class="cuatroColumnas">
												<strong>Nombre</strong>
											</p>
											<p class="cuatroColumnas">
												<strong>Descripci&oacute;n</strong>
											</p>
											
											<p class="cuatroColumnas">
												<strong>Editar</strong>
											</p>
											<p class="cuatroColumnas">
												<strong>Eliminar</strong>
											</p>
										</div>
										<!--Ciclo para cargar datos referencia -->
										<%
											DaoProgram program = new DaoProgram();
											program.conectar();
											ResultSet rs = program.getAllData();
										%>
										<div class='filaTitularesDescriptivosCuatro'>
											<div class="cuatroColumnas">
												<%=rs.getString("name").toUpperCase()%>
											</div>
											<div class="cuatroColumnas">
												<%=rs.getString("description")%>
											</div>
											<div class="cuatroColumnas">
												<form method='post' action='EditProgramSelect'>
													<input type='hidden'
														value="<%=rs.getString("programasID")%>" name='id'>
													<!--input type='submit' class="botonPequeno" value='Editar'-->
													<button type="submit" class="botonPequeno"
														id="botonEspacio">
														<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
													</button>
												</form>
											</div>
											<div class="cuatroColumnas">
												<form method='post' action='DeleteProgram' onsubmit="return confirm('�Desea eliminar el registro seleccionado?');">
													<input type='hidden'
														value="<%=rs.getString("programasID")%>" name='id'>
													<!--input type='submit' class="botonPequeno" value='Eliminar'-->
													<button type="submit" class="botonPequeno"
														id="botonEspacio">
														<i class="fa fa-times" aria-hidden="true"></i>
													</button>
												</form>
											</div>
										</div>
										<%
											while (rs.next()) {
										%>

										<div class='filaTitularesDescriptivosCuatro'>
											<div class="cuatroColumnas">
												<%=rs.getString("name").toUpperCase()%>
											</div>
											<div class="cuatroColumnas">
												<%=rs.getString("description")%>
											</div>
											<div class="cuatroColumnas">
												<form method='post' action='EditProgramSelect'>
													<input type='hidden'
														value="<%=rs.getString("programasID")%>" name='id'>
													<!--input type='submit' class="botonPequeno" value='Editar'-->
													<button type="submit" class="botonPequeno"
														id="botonEspacio">
														<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
													</button>
												</form>
											</div>
											<div class="cuatroColumnas">
												<form method='post' action='DeleteProgram' onsubmit="return confirm('�Desea eliminar el registro seleccionado?');">
													<input type='hidden'
														value="<%=rs.getString("programasID")%>" name='id'>
													<!--input type='submit' class="botonPequeno" value='Eliminar'-->
													<button type="submit" class="botonPequeno"
														id="botonEspacio">
														<i class="fa fa-times" aria-hidden="true"></i>
													</button>
												</form>
											</div>
										</div>
										<%
											}
											program.desconectar();
										%>
										<!--Fin del Ciclo cargar datos referencia -->
									</div>
								</div>
								<!--TERMINAN LAS PESTA�AS -->
							</div>
						</div>
						<br>
					</div>
				</div>
			</article>
			<div class="footer"></div>
		</section>
	</div>
	<!--TERMINA EL CONTENIDO AL 100% -->

	<!--EMPIEZA LOS BOTONES 100% -->


	<!--ACABA EL WRAPPER GENERAL -->
	<script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
	<script src="js/pesta�as.js"></script>
	<script>
		//alert("window.screen.availWidth" + window.screen.availWidth);
		if (window.screen.availWidth <= 1366) {
			document.body.style.zoom = "90%";

		}
		$(function() {

			var $body = $(document);
			$body.bind('scroll', function() {
				// "Desactivar" el scroll horizontal
				if ($body.scrollLeft() !== 0) {
					$body.scrollLeft(0);
				}
			});

		});
	</script>
</body>
<script>
	var currentTime = new Date();
	var year = currentTime.getFullYear();
	document.getElementById("anioActualx").innerHTML = year;
</script>
</html>
<%
	session.setAttribute("error", "");
%>