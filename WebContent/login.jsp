<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%-- En caso de que exista una sesion iniciada redirecciono a index.jsp. "NO tiene caso mostrar este formulario cuando hay una sesion iniciada --%>
<!--c:if test="${sessionScope['sessionEmail']!=null}">
    <!--% //response.sendRedirect("login.jsp");%-->
<!-- c:if-->

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Home Sentry</title>

<link href="loginFiles/css/bootstrap.min.css" rel="stylesheet">
<link href="loginFiles/css/datepicker3.css" rel="stylesheet">
<link href="loginFiles/css/styles.css" rel="stylesheet">
        <link rel="shortcut icon" type="image/png" href="http://cdn.totalcode.com/homesentry/images/banners/favicon.png" />
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->
</head>
<body>

	<div class="row">

		<div
			class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4"
			align="center" style="background-color: #FFFFFF;">
			<img src="loginFiles/images/hsentry.png" />
		</div>
		<div
			class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4 "
			style="background-color: #FFFFFF;">
			<div class="login-panel panel panel-default">
				<div class="panel-body">
				<p style="color: #ff0000">${sessionScope['error']}</p>
					<form role="form" action="LoginUser" method="post">
						<fieldset>
							<div class="form-group">
								<h4 class="text">Email</h4>
								<input class="form-control" placeholder="Email" id="email"
									name="email" type="email" value="" autocomplete="off">
							</div>
							<div class="form-group">
								<h4 class="text"> Password</h4>
								<input class="form-control" placeholder="Password" id="password" name="password" type="password" >
							</div>
							<div class="form-group">
								
									<input type="submit" value="Entrar" class="btn btn-primary">
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
		<!-- /.col-->
	</div>
	<!-- /.row -->



	<script src="loginFiles/js/jquery-1.11.1.min.js"></script>
	<script src="loginFiles/js/bootstrap.min.js"></script>
	<script src="loginFiles/js/chart.min.js"></script>
	<!-- <script src="loginFiles/js/chart-data.js"></script> -->
	<script src="loginFiles/js/easypiechart.js"></script>
	<!-- <script src="loginFiles/js/easypiechart-data.js"></script>
	<script src="loginFiles/js/bootstrap-datepicker.js"></script> -->
	<script>
		!function($) {
			$(document)
					.on(
							"click",
							"ul.nav li.parent > a > span.icon",
							function() {
								$(this).find('em:first').toggleClass(
										"glyphicon-minus");
							});
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function() {
			if ($(window).width() > 768)
				$('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function() {
			if ($(window).width() <= 767)
				$('#sidebar-collapse').collapse('hide')
		})
	</script>
</body>

</html>
<%
	session.setAttribute("error", "");
%>