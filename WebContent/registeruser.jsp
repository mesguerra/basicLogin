<%@taglib prefix="t" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="beans.*"%>
<%@page import="java.sql.ResultSet"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%-- En caso de que exista una sesion iniciada redirecciono a index.jsp. "NO tiene caso mostrar este formulario cuando hay una sesion iniciada --%>
<t:if test="${sessionScope['sessionEmail']==null}">
	<%
		response.sendRedirect("login.jsp");
	%>
</t:if>
<t:if test="${sessionScope['sessionIsAdmin']=='N'}">
	<%
	session.setAttribute("error", "Su usuario esta intentando ingresar a un perfil no autorizado");	
	response.sendRedirect("login.jsp");
	%>
</t:if>
<html class="" lang="es-ES">
<head>
<title>Home Sentry</title>
<meta name="description" content="Aca va la descripci&oacute;n" />
<meta charset="ISO-8859-1" />
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
<link rel="stylesheet" href="css/normalize.css">
<link rel="stylesheet" href="css/lightbox.css">
<link rel="stylesheet" href="css/boto.css">
<link rel="stylesheet" href="css/tooltip.css">
<link rel="shortcut icon" type="image/png"
	href="http://cdn.totalcode.com/homesentry/images/banners/favicon.png" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700"
	rel="stylesheet">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<script src="js/validar.js"></script>
<link href="https://fonts.googleapis.com/css?family=Oswald"
	rel="stylesheet">
<script src="js/funciones.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.10.2/validator.min.js"></script>
</head>
<body style="overflow: none !important;">
	<!--EMPIEZA EL WRAPPER GENERAL -->
	<div id="wrapperGeneral">
		<section class="columnaPrincipal">
			<article class="textInformativo">
				<!--EMPIEZA EL HEADER DEL TEXTO AL 100% -->
				<div class="textHeader">
					<div class="contenidoTextHeader">
						<div class="contenedorLogo">
							<div class="logoFoto"></div>
						</div>
						<div class="columText">
							<div class="filaBlanca">
								<p class="bienvenido">
									<strong>Bienvenidos</strong> - Home Sentry Calle 127
								</p>
							</div>
							<div class="filaGris">
								<p class="nomFactura">
									<strong>Administraci&oacute;n</strong>
								</p>
							</div>
							<div class="filaBlanca">
								<p class="nrofacturaa">
									<strong>Crear Usuario</strong>
								</p>
							</div>
						</div>
						<div class="columText">
							<div class="filaBlanca">
								<p class="anoFac" name="ano">
									<strong>A&ntilde;o: </strong><strong id="anioActualx">
										WXYZ</strong>
								</p>
							</div>
							<div class="filaGris">
								<p class="atendido">
									<strong>Usuario: </strong>
									${sessionScope['sessionNombre'].toUpperCase()}
								</p>
							</div>
							<div class="filaBlanca">
								<p class="fechaaa" id="fechaDinamica"
									style="font-family: 'Source Sans Pro', sans-serif;">
									<%
										Date dNow = new Date();
										SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
										String currentDate = ft.format(dNow);
										out.println(currentDate);
									%>
								</p>

								<p class="hora"
									style="font-family: 'Source Sans Pro', sans-serif;">
									<%
										SimpleDateFormat ft2 = new SimpleDateFormat("hh:mm aa");
										currentDate = ft2.format(dNow);
										out.println(currentDate);
										SimpleDateFormat ft3 = new SimpleDateFormat("yyyy");
										currentDate = ft3.format(dNow);
									%>
								</p>


							</div>
						</div>
					</div>
				</div>
				<!--TERMINA EL HEADER DEL TEXTO AL 100% -->
				<hr>
				<!--EMPIEZA EL HEADER 2 -->

				<form action="RegisterUser" method="post">

					<div class="textHeaderDosGrande" style="height: 100% !important;">
						<div class="contentHeaderDos">
							<div class="columnasTresContenidoCentrado">
								<div class="textFilaContenido">

									<div class="parrafofilaUno">
										<p style="color: #ff0000">${sessionScope['error']}</p>
									</div>
									<p class="parrafofilaDos">&nbsp;</p>
								</div>
								<div class="textFilaContenido">
									<div class="parrafofilaUno">Nombre:</div>
									<div class="parrafofilaDos">

										<input type="text" autocomplete="off" name="name" id="name" class="textField"
											pattern="[a-zA-Z0-9�-� ]*" required autofocus
											style="height: 22px;" /> <label class="labelObligatorio">*</label>
									</div>
								</div>
								<div class="textFilaContenido">
									<p class="parrafofilaUno">Email:</p>
									<p class="parrafofilaDos">
										<input autocomplete="off" type="email" name="email" id="email" class="textField"
											required autofocus style="height: 22px;"> <label
											class="labelObligatorio">*</label>
									</p>
								</div>
								<div class="textFilaContenido">
									<p class="parrafofilaUno">Contrase&ntilde;a:</p>
									<p class="parrafofilaDos">
										<input type="password" name="password1" id="password1"
											class="textField" required style="height: 22px;"><label
											class="labelObligatorio">*</label>
									</p>
								</div>
								<div class="textFilaContenido">
									<p class="parrafofilaUno">Confirmar Contrase&ntilde;a:</p>
									<p class="parrafofilaDos">
										<input type="password" name="password2" id="password2"
											class="textField" required style="height: 22px;"><label
											class="labelObligatorio">*</label>
									</p>
								</div>
								<div class="textFilaContenido">
									<p class="parrafofilaUno">Es Administrador:</p>
									<p class="parrafofilaDos">
										<select name="isadmin" class="desplegable">
											<option value="S">SI</option>
											<option value="N">NO</option>
										</select> <label class="labelObligatorio">*</label>
									</p>
								</div>
								<div class="textFilaContenido">
									<p class="parrafofilaUno">Perfil:</p>
									<p class="parrafofilaDos">
										<select name="profile" class="desplegable desplegableSucursales">
											<%
												DaoProfile profile = new DaoProfile();
												profile.conectar();
												ResultSet rs2 = profile.getAllData();
											%>
											<option value="<%=rs2.getString("perfilID")%> ">
												<%=rs2.getString("name") + " - "
					+ rs2.getString("description")%>
											</option>
											<%
												while (rs2.next()) {
											%>
											<option value="<%=rs2.getString("perfilID")%> ">
												<%=rs2.getString("name") + " - "
					+ rs2.getString("description")%>
											</option>
											<%
												}
												profile.desconectar();
											%>
										</select> <label class="labelObligatorio">*</label>
									</p>
								</div>
								<div class="botones">
									<button type="submit" class="botonPequeno" id="botonEspacio" 
									style="height: 39px;width: 120px;">
										<i class="fa fa-cogs" aria-hidden="true"></i>
									</button>
									<a href="usersadmin.jsp" class="botonPequeno" id="botonEspacio"
										style="background-color: #eb1c24; text-decoration: none;">
										<i class="" aria-hidden="true">Regresar</i>
									</a>
								</div>

							</div>
						</div>


					</div>
					<!--TERMINA EL CONTENIDO AL 100% -->

					<!--EMPIEZA LOS BOTONES 100% -->
				</form>
				<!--COMIENZA LIGHTBOX -->
				<!--TERMINA LIGHTBOX -->
			</article>
			<div class="footer"></div>
		</section>
	</div>
	<!--ACABA EL WRAPPER GENERAL -->
	<script src=""></script>
	<script src="js/pestanias.js"></script>
	<script src="js/readmore.js"></script>



	<script>
		if (window.screen.availWidth <= 1366) {
			document.body.style.zoom = "90%";
		}
		$(function() {
			var $body = $(document);
			$body.bind('scroll', function() {
				if ($body.scrollLeft() !== 0) {
					$body.scrollLeft(0);
				}
			});

		});
	</script>
</body>


<script>
	var currentTime = new Date();
	var year = currentTime.getFullYear();
	document.getElementById("anioActualx").innerHTML = year;
</script>
</html>
<%
	session.setAttribute("error", "");
%>