<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page isErrorPage="true"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%-- En caso de que exista una sesion iniciada redirecciono a index.jsp. "NO tiene caso mostrar este formulario cuando hay una sesion iniciada --%>
<!--c:if test="${sessionScope['sessionEmail']!=null}">
    <!--% //response.sendRedirect("login.jsp");%-->
<!-- c:if-->

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Home Sentry</title>
<%
	if ((request.getAttribute("javax.servlet.forward.request_uri")
			.toString().toLowerCase().contains("jsp"))) {
%>
<link href="../loginFiles/css/bootstrap.min.css" rel="stylesheet">
<link href="../loginFiles/css/datepicker3.css" rel="stylesheet">
<link href="../loginFiles/css/styles.css" rel="stylesheet">
<%
	} else if ((request.getAttribute("javax.servlet.forward.request_uri")
			.toString().substring(request.getAttribute("javax.servlet.forward.request_uri")
					.toString().length()-1).equals("/")))  {
%>
<link href="../loginFiles/css/bootstrap.min.css" rel="stylesheet">
<link href="../loginFiles/css/datepicker3.css" rel="stylesheet">
<link href="../loginFiles/css/styles.css" rel="stylesheet">
<%
	} else {
%>
<link href="./loginFiles/css/bootstrap.min.css" rel="stylesheet">
<link href="./loginFiles/css/datepicker3.css" rel="stylesheet">
<link href="./loginFiles/css/styles.css" rel="stylesheet">
<%
	}
%>

<link rel="shortcut icon" type="image/png"
	href="http://cdn.totalcode.com/homesentry/images/banners/favicon.png" />
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->
</head>
<body>

	<div class="row">

		<div
			class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4"
			align="center" style="background-color: #FFFFFF;">
			<%
				if (request.getAttribute("javax.servlet.forward.request_uri")
						.toString().toLowerCase().contains("jsp")) {
			%>
			<img src="../loginFiles/images/hsentry.png" />
			<%
	} else if ((request.getAttribute("javax.servlet.forward.request_uri")
			.toString().substring(request.getAttribute("javax.servlet.forward.request_uri")
					.toString().length()-1).equals("/")))  {
%>
			<img src="../loginFiles/images/hsentry.png" />
			<%
				} else {
			%>
			<img src="./loginFiles/images/hsentry.png" />
			<%
				}
			%>

		</div>
		<div
			class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4 "
			style="background-color: #FFFFFF;">
			<div class="login-panel panel panel-default">
				<div class="panel-body">
					<fieldset>
						<div class="form-group">
							<h4>Se ha presentado un error, favor comuniquese con el area
								de Tecnologia</h4>
							<h4>

								<p>
									<b>Error :</b> ${pageContext.errorData.statusCode}
								</p>
								<p>
									<b>Request URI:</b>
									${pageContext.request.scheme}://${header.host}${pageContext.errorData.requestURI}
								</p>
							</h4>
						</div>

					</fieldset>
				</div>
			</div>
		</div>
		<!-- /.col-->
	</div>
	<!-- /.row -->
	<%
	if (request.getAttribute("javax.servlet.forward.request_uri")
			.toString().toLowerCase().contains("jsp")) {
%>
<script src="../loginFiles/js/jquery-1.11.1.min.js"></script>
	<script src="../loginFiles/js/bootstrap.min.js"></script>
	<script src="../loginFiles/js/chart.min.js"></script>
	<!-- <script src="loginFiles/js/chart-data.js"></script> -->
	<script src="../loginFiles/js/easypiechart.js"></script>
	<!-- <script src="loginFiles/js/easypiechart-data.js"></script>
	<script src="loginFiles/js/bootstrap-datepicker.js"></script> -->
<%
	} else if ((request.getAttribute("javax.servlet.forward.request_uri")
			.toString().substring(request.getAttribute("javax.servlet.forward.request_uri")
					.toString().length()-1).equals("/")))  {
%>
<script src="../loginFiles/js/jquery-1.11.1.min.js"></script>
	<script src="../loginFiles/js/bootstrap.min.js"></script>
	<script src="../loginFiles/js/chart.min.js"></script>
	<!-- <script src="loginFiles/js/chart-data.js"></script> -->
	<script src="../loginFiles/js/easypiechart.js"></script>
	<!-- <script src="loginFiles/js/easypiechart-data.js"></script>
	<script src="loginFiles/js/bootstrap-datepicker.js"></script> -->
<%
	} else {
%>
<script src="./loginFiles/js/jquery-1.11.1.min.js"></script>
	<script src="./loginFiles/js/bootstrap.min.js"></script>
	<script src="./loginFiles/js/chart.min.js"></script>
	<!-- <script src="loginFiles/js/chart-data.js"></script> -->
	<script src="./loginFiles/js/easypiechart.js"></script>
	<!-- <script src="loginFiles/js/easypiechart-data.js"></script>
	<script src="loginFiles/js/bootstrap-datepicker.js"></script> -->
<%
	}
%>
	<script>
		!function($) {
			$(document)
					.on(
							"click",
							"ul.nav li.parent > a > span.icon",
							function() {
								$(this).find('em:first').toggleClass(
										"glyphicon-minus");
							});
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function() {
			if ($(window).width() > 768)
				$('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function() {
			if ($(window).width() <= 767)
				$('#sidebar-collapse').collapse('hide')
		})
	</script>
</body>

</html>
<%
	session.setAttribute("error", "");
%>